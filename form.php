<head>
        
 <meta name="page" content="text/html: charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <title> Задание 6  </title>
<style>
body {background: #FFC765;   color: #707981;}





.form-left-decoration:before,

.form-right-decoration:before,

.form-left-decoration:before {top: -20px;}

.form-right-decoration:before {
  top: -20px;
  right: 0;
}


.form-inner {padding: 50px;}
.form-inner input,
.form-inner textarea {
  display: block;
  width: 100%;
  padding: 0 20px;
  margin-bottom: 10px;
  background: #E9EFF6;
  line-height: 40px;
  border-width: 0;
  border-radius: 20px;
  font-family: 'Сomic Sans MS', sans-serif;
}


.form-inner input[type="submit"] {
  margin-top: 25px;
  background: #F5BD78;
  border-bottom: 5px solid #F5BD78;
  color: white;
  font-size: 14px;
}


.form-inner textarea {resize: none;}
.form-inner h3 {
  margin-top: 0;
  font-family: 'Сomic Sans MS', sans-serif;
  font-weight: bold;
  font-size: 24px;
  color: #707981;
}

.error {
border: 2px solid red;}


a.butt {
        
        font-size:13px;
        text-decoration: none;
        font-weight: 700;
       padding: 3px 6px;
        background: #eaeef1;
        
        width:60px;
       
        background-image: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,.1));
        border-radius: 3px;
        color: rgba(0,0,0,.6);
        text-shadow: 0 1px 1px rgba(255,255,255,.7);
        box-shadow: 0 0 0 1px rgba(0,0,0,.2), 0 1px 2px rgba(0,0,0,.2), inset 0 1px 2px rgba(255,255,255,.7);
}

a.butt:hover, a.butt.hover {
        background: #fff;
}
a.butt:active, a.butt.active {
        background: #d0d3d6;
        background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,0));
        box-shadow: inset 0 0 2px rgba(0,0,0,.2), inset 0 2px 5px rgba(0,0,0,.2), 0 1px rgba(255,255,255,.2);
}



</style>
  
    </head>
 <body >   
 <?php
    if (!empty($messages)) {
      print('<div id="messages">');
      // Выводим все сообщения.
      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    } ?>
    
 <div class=" container" align="center" style="padding-top:40px; padding-bottom:40px;">
 <p style="color:#fff; font-size:30px;">Вход за администратора,  <a href="admin.php" class="butt">нажми.</a></p>.
 
<form action="" method="POST" class="decor">
  <div class="form-left-decoration"></div>
  <div class="form-right-decoration"></div>
  <div class="circle"></div>
<div class="form-inner">
<h3>Форма</h3>
<table>
<tr>
<th>
 <div <?php if ($errors['name']) {print 'class="error"';} ?>> <input name="name" value="<?php print $values['name']; ?>" placeholder="FIO"/></div><br/>


<div <?php if ($errors['email']) {print 'class="error"';} ?>> <input name="email" value="<?php print $values['email']; ?>" placeholder="test@mail.ru" /></div>

<br />

<div <?php if ($errors['fieldname']) {print 'class="error"';} ?>> <textarea name="fieldname" placeholder="Tell about yourself" value="<?php print $values['fieldname']; ?>"></textarea></div>

<br />

  Дата рождения <br/>&emsp;&emsp;&emsp;
  <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>"> 
  <?php  for ($i=2020;$i>1900;$i--) {?>
  <option value="<?php print($i);?>"><?php print($i);?></option>
  <?php } ?>
  </select>
  <br />
  </th>
        
        <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</th>
        
        <th>
       
  &emsp;<div  <?php if ($errors['sex']) {print 'class="error"';} ?>>
      <label><input type="radio"  
        name="sex" value="Male" />
        Мужщина</label>&emsp;&emsp;&emsp;
      <label><input type="radio" 
        name="sex" value="Female" />
        Женщина</label></div><br /><br />
        Кол-во конечностей.
       <br />&emsp;
     <div <?php if ($errors['limbs']) {print 'class="error"';} ?>> <label><input type="radio" 
        name="limbs" value="One" />
        1</label>&emsp;&emsp;
      <label><input type="radio" 
        name="limbs" value="Two" />
        2</label>&emsp;&emsp;
        <label><input type="radio" 
        name="limbs" value="Three" />
        3</label>&emsp;&emsp;
        <label><input type="radio" 
        name="limbs" value="Four" />
        4</label></div>
        <br /><br />
       Способность <br />
   <select name="abilities" multiple <?php if ($errors['abilities']) {print 'class="error"';} ?>> 
  <option value="Immotality">Бесмертие</option>
          <option value="Passage throw walls" >Прохождение сквозь стены</option>
          <option value="Levitation" >Левитация</option>
          <option value="Mind reading">Чтение мыслей</option>
          <option value="Hyperspeed" >Супер скорость</option>
          <value="<?php print $values['abilities']; ?>">
  </select>

      </th>
      </tr>
      </table>
      <label><input type="checkbox" 
        name="checks" />
      <div <?php if ($errors['checks']) {print 'class="error"';} ?>>  Я согласен/а.</label></div>
      <br /><br />
  <input  type="submit" value="Отправить" />
  </div>
</form>
</div>
</body>
